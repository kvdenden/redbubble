require 'bundler'

ENV['RACK_ENV'] ||= ENV.fetch('RACK_ENV', 'development')
Bundler.require(:default, ENV['RACK_ENV'].to_sym)

%w(. ./lib).each do |p|
  $LOAD_PATH.unshift File.expand_path(p, File.dirname(__FILE__))
end

require 'logger'
require 'active_support/core_ext/hash'

require 'helpers/timestamped'
require 'validators/work_validator'
require 'models/work'
require 'models/camera_model'
require 'models/camera_make'
require 'factories/works_factory'
require 'repositories/works_repository'

require 'models/page'
require 'models/index_page'
require 'representers/page_representer'

require 'transforms/reject_invalid_works'
require 'transforms/group_works_per_camera_make'
require 'transforms/generate_pages'
require 'transforms/add_page_representers'

require 'destinations/static_site_generator'

require 'batch_processor'
require 'runner'
