RSpec.describe PageRepresenter do
  include RSpecHtmlMatchers

  let(:title) { 'NIKON D80' }
  let(:navigation) { { 'index' => '/', 'make' => '/nikon' } }
  let(:thumbnails) { %w(thumb1.jpg thumb2.jpg thumb3.jpg) }

  let(:page) { Page.new(title, navigation: navigation, thumbnails: thumbnails) }
  subject(:representer) { PageRepresenter.create(page) }

  describe '#render' do
    let(:html) { representer.render }

    it 'renders the title of the html document' do
      expect(html).to have_tag('title', text: title)
    end

    it 'renders the title in the header' do
      expect(html).to have_tag('header') do
        with_tag('h1', text: title)
      end
    end

    it 'renders the navigation as links in the header' do
      navigation.each do |text, url|
        expect(html).to have_tag('header') do
          with_tag('nav') do
            with_tag('a', text: text, with: { href: url })
          end
        end
      end
    end

    it 'renders the thumbnails as images' do
      thumbnails.each do |url|
        expect(html).to have_tag('img', with: { src: url })
      end
    end
  end
end
