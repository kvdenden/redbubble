RSpec.describe WorksRepository do
  let(:repository_url) { 'http://foo.bar' }
  let(:response) { File.read('./spec/support/works.xml') }
  let(:factory) { double('factory') }
  let(:work) { double('work') }
  subject(:repository) { WorksRepository.new(repository_url, factory: factory) }

  describe 'works' do
    before { allow(factory).to receive(:call).and_return(work) }

    context 'when valid repository url' do
      before { stub_request(:get, repository_url).to_return(response) }

      it 'contains the correct number of works' do
        expect(repository.works.count).to eq(14)
      end

      it 'delegates work creation to factory' do
        expect(factory).to receive(:call).exactly(14).times
        repository.works
      end

      it 'returns the works created by the factory' do
        expect(repository.works.first).to eq(work)
      end
    end

    context 'when request raises an error' do
      before { stub_request(:get, repository_url).to_raise('error') }

      it 'raises an error' do
        expect { repository.works }.to raise_error('error')
      end
    end

    context 'when request has invalid status code' do
      before { stub_request(:get, repository_url).to_return(status: 404) }

      it 'raises an error' do
        expect { repository.works }.to raise_error(ArgumentError)
      end
    end
  end
end
