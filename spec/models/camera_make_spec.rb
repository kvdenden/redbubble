RSpec.describe CameraMake do
  let(:nikon) { 'NIKON CORPORATION' }
  let(:work) { double('work') }
  let(:model) { double('model', works: [work]) }
  subject(:make) { CameraMake.new(nikon) }

  context 'new make' do
    its(:name) { is_expected.to eq(nikon) }
    its(:models) { is_expected.to be_empty }
    its(:works) { is_expected.to be_empty }
  end

  context 'make contains models' do
    before { make.models << model }

    its(:models) { is_expected.to eq([model]) }
    its(:works) { is_expected.to eq([work]) }
  end
end
