RSpec.describe Work do
  let(:nikon) { 'NIKON CORPORATION' }
  let(:nikon_d80) { 'NIKON D80' }
  let(:thumbnails) do
    {
      small:  'http://ih1.redbubble.net/work.31820.1.flat,135x135,075,f.jpg',
      medium: 'http://ih1.redbubble.net/work.31820.1.flat,300x300,075,f.jpg',
      large:  'http://ih1.redbubble.net/work.31820.1.flat,550x550,075,f.jpg'
    }
  end
  subject(:work) { Work.new(make: nikon, model: nikon_d80, thumbnails: thumbnails) }

  its(:thumbnails) { is_expected.to eq(thumbnails) }
  its(:make) { is_expected.to eq(nikon) }
  its(:model) { is_expected.to eq(nikon_d80) }

  it 'works are sorted by created time' do
    work1 = Work.new(make: nikon, model: nikon_d80, thumbnails: thumbnails)
    work2 = Work.new(make: nikon, model: nikon_d80, thumbnails: thumbnails)
    expect([work2, work1].sort).to eq([work1, work2])
  end

  describe 'validation' do
    let(:make) { nikon }
    let(:model) { nikon_d80 }
    subject(:work) { Work.new(make: make, model: model, thumbnails: thumbnails) }

    it 'is valid when it has a make and model' do
      expect(work.valid?).to be true
    end

    context 'when make is nil' do
      let(:make) { nil }
      it { expect(work.valid?).to be false }
    end

    context 'when make is empty string' do
      let(:make) { '' }
      it { expect(work.valid?).to be false }
    end

    context 'when model is nil' do
      let(:model) { nil }
      it { expect(work.valid?).to be false }
    end

    context 'when model is empty string' do
      let(:model) { '' }
      it { expect(work.valid?).to be false }
    end
  end
end
