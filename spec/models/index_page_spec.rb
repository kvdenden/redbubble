RSpec.describe IndexPage do
  subject(:page) { IndexPage.new('Home') }

  its(:path) { is_expected.to eq('/') }
end
