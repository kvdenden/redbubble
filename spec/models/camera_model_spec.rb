RSpec.describe CameraModel do
  let(:nikon) { double('make') }
  let(:nikon_d80) { 'NIKON D80' }
  let(:work) { double('work') }
  subject(:model) { CameraModel.new(nikon_d80, make: nikon) }

  context 'new model' do
    its(:name) { is_expected.to eq(nikon_d80) }
    its(:make) { is_expected.to eq(nikon) }
    its(:works) { is_expected.to be_empty }
  end

  context 'model contains works' do
    before { model.works << work }

    its(:works) { is_expected.to eq([work]) }
  end
end
