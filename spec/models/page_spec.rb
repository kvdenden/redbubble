RSpec.describe Page do
  let(:title) { 'NIKON D80' }
  let(:navigation) { { 'index' => '/', 'make' => '/nikon' } }
  let(:thumbnails) { %w(thumb1.jpg thumb2.jpg thumb3.jpg) }

  context 'without parent' do
    subject(:page) { Page.new(title, navigation: navigation, thumbnails: thumbnails) }

    its(:title) { is_expected.to eq(title) }
    its(:navigation) { is_expected.to eq(navigation) }
    its(:thumbnails) { is_expected.to eq(thumbnails) }

    its(:slug) { is_expected.to eq('nikon-d80') }
    its(:path) { is_expected.to eq('/nikon-d80.html') }
  end

  context 'with parent' do
    let(:index_page) { Page.new('Home', is_index: true) }
    subject!(:page) { Page.new(title, thumbnails: thumbnails, parent: index_page) }

    its(:navigation) { is_expected.to eq(index_page.title => index_page.path) }
    its(:path) { is_expected.to start_with(index_page.path) }

    it 'parent navigation contains link to child' do
      expect(index_page.navigation).to eq(page.title => page.path)
    end
  end

  context 'title with special characters' do
    let(:title) { 'FUJI PHOTO FILM CO., LTD.' }
    subject(:page) { Page.new(title) }
    its(:slug) { is_expected.to eq('fuji-photo-film-co-ltd') }
  end
end
