RSpec.describe WorksFactory do
  let(:nikon) { 'NIKON CORPORATION' }
  let(:nikon_d80) { 'NIKON D80' }
  let(:small) do
    double(attributes: { 'type' => 'small' }, to_s: 'http://ih1.redbubble.net/work.31820.1.flat,135x135,075,f.jpg')
  end
  let(:large) do
    double(attributes: { 'type' => 'large' }, to_s: 'http://ih1.redbubble.net/work.31820.1.flat,550x550,075,f.jpg')
  end
  let(:attrs) do
    {
      exif: { make: nikon, model: nikon_d80 },
      urls: { url: [small, large] }
    }
  end

  let(:factory) { WorksFactory.new }
  subject(:work) { factory.call(attrs) }

  its(:thumbnails) { is_expected.to eq(small: small.to_s, large: large.to_s) }
  its(:make) { is_expected.to eq(nikon) }
  its(:model) { is_expected.to eq(nikon_d80) }
end
