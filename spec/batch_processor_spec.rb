RSpec.describe BatchProcessor do
  let(:work1) { double('work1') }
  let(:intermediate) { double('intermediate result') }
  let(:page1) { double('page1') }
  let(:page2) { double('page2') }

  let(:repository) { double('repository') }
  let(:destination) { double('destination') }
  let(:step1) { double('step1') }
  let(:step2) { double('step2') }
  subject(:processor) { BatchProcessor.new(source: [work1], destination: destination, steps: [step1, step2]) }

  before do
    allow(step1).to receive(:call).with([work1]).and_return(intermediate)
    allow(step2).to receive(:call).with(intermediate).and_return([page1, page2])
  end

  describe '#run!' do
    it 'sends the transformed results to the destination' do
      expect(destination).to receive(:write!).with(page1).once
      expect(destination).to receive(:write!).with(page2).once
      processor.run!
    end
  end
end
