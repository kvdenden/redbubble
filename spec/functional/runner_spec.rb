RSpec.describe Runner do
  let(:api_url) { 'http://works.xml' }
  let(:api_response) { File.read('./spec/support/sample_works.xml') }
  let(:output_directory) { '/tmp/bubble-works' }

  before do
    stub_request(:get, api_url).to_return(api_response)
    FileUtils.rm_rf(output_directory)
  end

  let(:index_path) { output_directory + '/index.html' }
  let(:make_path)  { output_directory + '/nikon-corporation/index.html' }
  let(:model_path) { output_directory + '/nikon-corporation/nikon-d80.html' }
  let(:asset_path) { output_directory + '/style.css' }

  let(:run!) { Runner.run!(api_url: api_url, output_directory: output_directory) }

  it 'creates the index page' do
    expect { run! }.to change { File.file?(index_path) }.from(false).to(true)
  end

  it 'creates the camera make page' do
    expect { run! }.to change { File.file?(make_path) }.from(false).to(true)
  end

  it 'creates the camera model page' do
    expect { run! }.to change { File.file?(model_path) }.from(false).to(true)
  end

  it 'copies the required assets' do
    expect { run! }.to change { File.file?(asset_path) }.from(false).to(true)
  end
end
