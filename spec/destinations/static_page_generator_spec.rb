RSpec.describe StaticPageGenerator do
  let(:page) { Page.new('Foo') }
  let(:directory) { '/tmp/mysite' }
  subject(:generator) { StaticPageGenerator.new(directory) }
  let(:write!) { generator.write!(PageRepresenter.new(page, template: '')) }

  describe '#write!' do
    after do
      FileUtils.rm_rf(directory)
    end

    it 'creates the output directory' do
      expect { write! }.to change { File.directory?(directory) }.from(false).to(true)
    end

    it 'creates a new file with the right name' do
      expect { write! }.to change { File.file?(directory + page.path) }.from(false).to(true)
    end

    context 'when the path ends in a /' do
      let(:page) { IndexPage.new('Home') }

      it 'creates a new file with the right name' do
        expect { write! }.to change { File.file?(directory + '/index.html') }.from(false).to(true)
      end
    end
  end
end
