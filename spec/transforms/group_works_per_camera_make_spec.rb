RSpec.describe Transforms::GroupWorksPerCameraMake do
  let(:nikon) { 'NIKON CORPORATION' }
  let(:canon) { 'Canon' }
  let(:nikon_d80) { 'NIKON D80' }
  let(:eos_20d) { 'Canon EOS 20D' }
  let(:eos_400d) { 'Canon EOS 400D DIGITAL' }
  let(:work1) { Work.new(make: canon, model: eos_20d) }
  let(:work2) { Work.new(make: canon, model: eos_20d) }
  let(:work3) { Work.new(make: canon, model: eos_400d) }
  let(:work4) { Work.new(make: nikon, model: nikon_d80) }

  subject(:transform) { Transforms::GroupWorksPerCameraMake.new }

  describe '#call' do
    let(:makes) { transform.call([work1, work2, work3, work4]) }

    it 'returns the right number of makes' do
      expect(makes.count).to eq(2)
    end

    it 'returns an array of camera makes' do
      expect(makes).to all(be_a(CameraMake))
    end

    describe 'camera make' do
      let(:make) { makes.first }

      it 'contains the right name' do
        expect(make.name).to eq(canon)
      end

      it 'contains the right camera models' do
        expect(make.models.count).to eq(2)
      end

      it 'contains the right works' do
        expect(make.works).to eq([work1, work2, work3])
      end

      describe 'camera model' do
        let(:model) { make.models.first }

        it 'contains the right name' do
          expect(model.name).to eq(eos_20d)
        end

        it 'contains the right camera make' do
          expect(model.make).to eq(make)
        end

        it 'contains the right works' do
          expect(model.works).to eq([work1, work2])
        end
      end
    end
  end
end
