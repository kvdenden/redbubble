RSpec.describe Transforms::GeneratePages do
  let(:home) { 'Home' }
  let(:nikon) { 'NIKON CORPORATION' }
  let(:nikon_d80) { 'NIKON D80' }
  let(:thumbnail_size) { :small }
  let(:thumbs1) do
    {
      small:  'http://ih1.redbubble.net/work.31820.1.flat,135x135,075,f.jpg',
      medium: 'http://ih1.redbubble.net/work.31820.1.flat,300x300,075,f.jpg',
      large:  'http://ih1.redbubble.net/work.31820.1.flat,550x550,075,f.jpg'
    }
  end
  let(:thumbs2) do
    {
      small:  'http://ih1.redbubble.net/work.31821.1.flat,135x135,075,f.jpg',
      medium: 'http://ih1.redbubble.net/work.31821.1.flat,300x300,075,f.jpg',
      large:  'http://ih1.redbubble.net/work.31821.1.flat,550x550,075,f.jpg'
    }
  end
  let(:work1) { Work.new(make: nikon, model: nikon_d80, thumbnails: thumbs1) }
  let(:work2) { Work.new(make: nikon, model: nikon_d80, thumbnails: thumbs2) }
  let(:camera_model) { CameraModel.new(nikon_d80, works: [work1, work2]) }
  let(:camera_make) { CameraMake.new(nikon, models: [camera_model]) }

  subject(:transform) { Transforms::GeneratePages.new(thumbnail_size: thumbnail_size) }

  describe '#call' do
    let(:pages) { transform.call([camera_make]) }
    let(:index_page) { pages.find { |page| page.path == '/' } }
    let(:make_page) { pages.find { |page| page.path == '/nikon-corporation/' } }
    let(:model_page) { pages.find { |page| page.path == '/nikon-corporation/nikon-d80.html' } }

    it 'contains the right number of pages' do
      expect(pages.count).to eq(3)
    end

    it 'returns an array of page representers' do
      expect(pages).to all(be_a(Page))
    end

    describe 'index page' do
      it 'contains the right title' do
        expect(index_page.title).to eq(home)
      end

      it 'contains the right navigation' do
        expect(index_page.navigation).to eq(make_page.title => make_page.path)
      end

      it 'contains the right thumbnails' do
        expect(index_page.thumbnails).to eq([thumbs1[:small], thumbs2[:small]])
      end
    end

    describe 'make page' do
      it 'contains the right title' do
        expect(make_page.title).to eq(nikon)
      end

      it 'contains the right navigation' do
        expect(make_page.navigation).to eq(index_page.title => index_page.path, model_page.title => model_page.path)
      end

      it 'contains the right thumbnails' do
        expect(make_page.thumbnails).to eq([thumbs1[:small], thumbs2[:small]])
      end
    end

    describe 'model page' do
      it 'contains the right title' do
        expect(model_page.title).to eq(nikon_d80)
      end

      it 'contains the right navigation' do
        expect(model_page.navigation).to eq(index_page.title => index_page.path, make_page.title => make_page.path)
      end

      it 'contains the right thumbnails' do
        expect(model_page.thumbnails).to eq([thumbs1[:small], thumbs2[:small]])
      end
    end
  end
end
