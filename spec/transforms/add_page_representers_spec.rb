RSpec.describe Transforms::AddPageRepresenters do
  let(:page1) { double('page 1') }
  let(:page2) { double('page 2') }
  subject(:transform) { Transforms::AddPageRepresenters.new }

  describe '#call' do
    let(:wrapped_pages) { transform.call([page1, page2]) }

    it 'contains the same number of pages' do
      expect(wrapped_pages.count).to eq(2)
    end

    it 'returns an array of page representers' do
      expect(wrapped_pages).to all(be_a(PageRepresenter))
    end

    it 'returns representers with the original pages' do
      expect(wrapped_pages.map(&:page)).to eq([page1, page2])
    end
  end
end
