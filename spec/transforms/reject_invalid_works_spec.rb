RSpec.describe Transforms::RejectInvalidWorks do
  let(:valid_work) { Work.new(make: 'NIKON CORPORATION', model: 'NIKON D80') }
  let(:invalid_work) { Work.new(make: nil, model: nil) }

  subject(:transform) { Transforms::RejectInvalidWorks.new }

  describe '#call' do
    let(:valid_works) { transform.call([valid_work, invalid_work]) }

    it 'returns the valid works' do
      expect(valid_works).to eq([valid_work])
    end
  end
end
