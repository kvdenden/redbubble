## Bubble works

### Description
The Redbubble system has many digital images, often taken with a camera. EXIF data from a selection of these images is available via an API.

The API is available at: [/api/v1/works.xml](http://take-home-test.herokuapp.com/api/v1/works.xml)

Bubble works is a batch processor that takes data from the API, and produces a single HTML page (based on this output template), for each camera make, camera model and also an index page.

The batch processor takes the API URL and the output directory as parameters.

### Usage

#### Requirements

- Ruby 2.3
- `bundle install` all the gems

#### Running

`bin/bubble -u <api_url> -o <output_dir>`

eg. `bin/bubble -u "http://take-home-test.herokuapp.com/api/v1/works.xml" -o "/tmp/test-bubble"`

Alternatively you can use environment variables to pass in the arguments

```
export API_URL="http://example.com/api.xml"
export OUTPUT_DIRECTORY="/tmp/website"
bin/bubble
```
#### Testing

`rspec` to run the tests
