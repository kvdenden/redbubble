require 'net/http'

class WorksRepository
  class << self
    def create(uri)
      new(uri, factory: WorksFactory.new)
    end
  end

  attr_reader :works
  def initialize(uri, factory: :itself.to_proc)
    @uri = URI(uri)
    @factory = factory
  end

  def works
    @works ||= fetch_works
  end

  private

  def fetch_works
    logger.info("Fetching works from #{@uri}")
    response = Net::HTTP.get_response(@uri)

    raise ArgumentError, "Invalid repository url: #{@uri}" unless response.code == '200'

    doc = Nori.new.parse(response.body).deep_symbolize_keys
    works = Array.wrap(doc.dig(:works, :work))
    logger.info("#{works.count} works found")
    works.map do |work|
      @factory.call(work)
    end
  end

  def logger
    @logger ||= Logger.new(STDOUT)
  end
end
