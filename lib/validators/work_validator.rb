class WorkValidator
  include Veto.validator

  validates :make, presence: true
  validates :model, presence: true
end
