class Page
  attr_reader :title, :thumbnails, :parent
  attr_reader :parent_navigation, :child_navigation
  def initialize(title, navigation: {}, thumbnails: [], parent: nil, is_index: false)
    @title = title
    @child_navigation = navigation
    @thumbnails = thumbnails
    @parent = parent
    @is_index = is_index

    init_parent_navigation
  end

  def navigation
    parent_navigation.merge(child_navigation)
  end

  def slug
    title.downcase.strip.tr(' ', '-').gsub(/[^\w-]/, '')
  end

  def path
    prefix = (parent&.path || '/')
    suffix = @is_index ? '/' : '.html'

    prefix + slug + suffix
  end

  private

  def init_parent_navigation
    @parent_navigation = ancestors.map { |page| [page.title, page.path] }.to_h
    parent.child_navigation[title] = path if parent
  end

  def ancestors
    ancestors = []
    ancestor = parent
    while ancestor
      ancestors.unshift(ancestor)
      ancestor = ancestor.parent
    end
    ancestors
  end
end
