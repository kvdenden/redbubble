class CameraMake
  attr_reader :name, :models

  def initialize(name, models: [])
    @name = name
    @models = models
  end

  def works
    models.flat_map(&:works)
  end
end
