class Work
  attr_reader :make, :model, :thumbnails

  include Timestamped
  include Veto.model(WorkValidator.new)

  def initialize(make:, model:, thumbnails: {})
    @make = make
    @model = model
    @thumbnails = thumbnails
  end
end
