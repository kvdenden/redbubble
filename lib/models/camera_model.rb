class CameraModel
  attr_reader :name, :make, :works

  def initialize(name, make: nil, works: [])
    @name = name
    @make = make
    @works = works
  end
end
