class IndexPage < Page
  def initialize(title = 'Index', navigation: {}, thumbnails: [])
    super(title, navigation: navigation, thumbnails: thumbnails, parent: nil, is_index: true)
  end

  def path
    '/'
  end
end
