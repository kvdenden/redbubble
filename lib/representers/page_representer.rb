class PageRepresenter
  class << self
      def create(page)
        new(page,
          template: File.read('config/template/page.html.erb'),
          assets: {
            'config/template/style.css' => 'style.css',
            'config/template/script.js' => 'script.js',
             })
      end
  end

  attr_reader :page, :template, :assets

  delegate :path, to: :page

  def initialize(page, template:, assets: {})
    @page = page
    @template = template
    @assets = assets
  end

  def render
    ERB.new(template).result(binding)
  end

  alias to_s render
end
