class WorksFactory
  def call(attrs)
    # require 'byebug'; byebug
    thumbnails = attrs.dig(:urls, :url).map do |url|
      [url.attributes['type'].to_sym, url.to_s]
    end.to_h
    make = attrs.dig(:exif, :make)
    model = attrs.dig(:exif, :model)

    Work.new(make: make, model: model, thumbnails: thumbnails)
  end
end
