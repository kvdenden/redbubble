class Runner
  class << self
    def run!(api_url:, output_directory:)
      repository = WorksRepository.create(api_url)
      generator = StaticPageGenerator.new(output_directory)

      processor = BatchProcessor.new(source: repository.works, destination: generator, steps: steps)
      processor.run!
    end

    private

    def steps
      [
        Transforms::RejectInvalidWorks.new,
        Transforms::GroupWorksPerCameraMake.new,
        Transforms::GeneratePages.new,
        Transforms::AddPageRepresenters.new
      ]
    end
  end
end
