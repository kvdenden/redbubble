module Transforms
  class GeneratePages
    def initialize(thumbnail_size: :large)
      @thumbnail_size = thumbnail_size
    end

    def call(makes)
      index_page = generate_index_page(makes)
      other_pages = makes.flat_map do |make|
        make_page = generate_make_page(make, index_page: index_page)
        model_pages = make.models.map do |model|
          generate_model_page(model, make_page: make_page)
        end
        [make_page] + model_pages
      end
      [index_page] + other_pages
    end

    private

    def generate_index_page(makes)
      index_page = IndexPage.new('Home')
      add_thumbnails(index_page, makes.flat_map(&:works), limit: 10)
    end

    def generate_make_page(make, index_page:)
      make_page = Page.new(make.name, parent: index_page, is_index: true)
      add_thumbnails(make_page, make.works, limit: 10)
    end

    def generate_model_page(model, make_page:)
      model_page = Page.new(model.name, parent: make_page)
      add_thumbnails(model_page, model.works)
    end

    def add_thumbnails(page, works, limit: nil)
      works = works.sort.take(limit) if limit
      works.each do |work|
        thumbnails = work.thumbnails
        page.thumbnails << thumbnails.fetch(@thumbnail_size, thumbnails.values.first)
      end
      page
    end
  end
end
