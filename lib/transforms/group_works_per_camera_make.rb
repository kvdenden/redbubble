module Transforms
  class GroupWorksPerCameraMake
    def call(works)
      works.group_by(&:make).map do |make_name, make_works|
        camera_make = CameraMake.new(make_name)
        make_works.group_by(&:model).each do |model_name, model_works|
          camera_make.models << CameraModel.new(model_name, make: camera_make, works: model_works)
        end
        camera_make
      end
    end
  end
end
