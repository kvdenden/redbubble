module Transforms
  class AddPageRepresenters
    def call(pages)
      pages.map { |page| wrap_page(page) }
    end

    private

    def wrap_page(page)
      PageRepresenter.create(page)
    end
  end
end
