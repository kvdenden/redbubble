module Transforms
  class RejectInvalidWorks
    def call(works)
      works.select(&:valid?)
    end
  end
end
