class BatchProcessor
  attr_reader :source, :destination, :steps

  def initialize(source:, destination:, steps: [])
    @source = source
    @destination = destination
    @steps = steps
  end

  def run!
    steps.reduce(source) { |cur, step| step.call(cur) }.each do |result|
      destination.write!(result)
    end
  end
end
