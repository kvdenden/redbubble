module Timestamped
  module Initializer
    attr_reader :timestamp
    def initialize(*args)
      @timestamp = Time.now
      super(*args)
    end

    def <=>(other)
      timestamp <=> other.timestamp
    end
  end

  def self.included(klass)
    klass.send :prepend, Initializer
  end
end
