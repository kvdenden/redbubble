class StaticPageGenerator
  def initialize(output_directory)
    @output_directory = output_directory
  end

  def write!(page)
    path = @output_directory + find_path(page.path)

    begin
      FileUtils.mkdir_p(File.dirname(path))

      File.open(path, 'w') { |f| f.write(page.to_s) }
      page.assets.each do |source, asset|
        destination = File.join(@output_directory, asset)
        FileUtils.mkdir_p(File.dirname(destination))
        FileUtils.cp(source, destination) unless File.exist?(destination)
      end
    end
  end

  private

  def find_path(path)
    if path.end_with?('/')
      path + 'index.html'
    else
      path
    end
  end
end
